package pl.game.gamerpg.services;

import pl.game.gamerpg.model.ResultBattle;

public interface GameLogicService {

    void storyAboutAdventure();

    void battleWithThief();

    void battleWithGuard();

    void battleWithDragon();
}
